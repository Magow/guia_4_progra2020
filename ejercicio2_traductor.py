#  !/usr/bin/env python3
#   -*- coding:utf-8 -*-

from os import system


#   Busqueda de la palabra en el diccionario
def buscar_traduccion(dic_translate):
    print("Ingrese la palabra que desee traducir con el diccionario online")
    print('\n')
    word_sought = input("Palabra (en español): ")
    if word_sought in dic_translate:
        print("Traducción de la palabra: ", dic_translate[word_sought])
    else:
        print("La palabra ingresada no existe en el diccionario")
        print("Usted puede agregarla")


#    Creación de la lista de palabras
def list_maker(list_of_words):

    #   palabras para copiar
    #   de;of y;and yo;I para;for no;not perro;dog gato;cat hacer;do por;by

    print("Ingrese las palabras para el diccionario")
    print('\n')
    print("las palabrar originales y traducciones deben ser separadas por (;)")
    print("si es la palabra siguiente separe con un espacio")
    print("Siga las normas o el diccionario creado será un caos :]\n")
    list_of_words = input("Palabras: ")
    list_of_words = list_of_words.replace(" ", ";")
    list_of_words = list_of_words.split(";")
    for i in range(len(list_of_words)):
        list_of_words[i] = list_of_words[i].lower()

    return list_of_words


#   Creación de un diccionario en base a dos listas
def dic_convert(dic_translate, list_of_words):

    temp_keys = []
    temp_values = []

    for i in range(0, len(list_of_words), 2):
        temp_keys.append(list_of_words[i])
    for i in range(1, len(list_of_words), 2):
        temp_values.append(list_of_words[i])

    dic_translate = dict(zip(temp_keys, temp_values))

    return dic_translate


#   Menu de acciones
def menu():
    print("Menu de acciones")
    print('\n')
    print("(1) Buscar una palabra en el diccionario")
    print("(2) Agregar más palabras al diccionario")
    print("(3) Imprimir diccionario")
    print("(4) Salir del programa")

if __name__ == '__main__':

    list_of_words = []
    list_of_words = list_maker(list_of_words)

    dic_translate = {}
    dic_translate = dic_convert(dic_translate, list_of_words)

#   cilo while para lo que desee el usuario
    while True:
        menu()
        choice = input("Ingrese la acción que desee realizar: ")
        if choice == "1":
            system('clear')
            buscar_traduccion(dic_translate)
            print('\n')
        elif choice == "2":
            system('clear')
            print("Generar un nuevo diccionario")
            list_of_words = list_maker(list_of_words)
            dic_translate = dic_convert(dic_translate, list_of_words)
            print('\n')
        elif choice == "3":
            system('clear')
            print(dic_translate)
        elif choice == "4":
            system('clear')
            print("Ha salido del programa")
            print('\n')
            break
        else:
            system('clear')
            print("Opción no valida")
