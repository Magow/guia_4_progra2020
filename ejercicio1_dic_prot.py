#  !/usr/bin/env python3
#   -*- coding:utf-8 -*-
#    System para borrar terminal
from os import system


#   Busqueda de la proteína para imprimir su descripción
def print_description(prot_base):
    print("Ingrese el nombre de la proteína buscada")
    print('\n')
    prot_sought = input("Nombre: ")
    prot_sought = prot_sought.upper()
    if prot_sought in prot_base:
        print("Descripción de la proteína: ", prot_base[prot_sought])
    else:
        print("la proteína ingresada no existe en la base de datos")
    print('\n')


#   Agregar un elemento nuevo a la base de datos
def add_elements(prot_base):
    print("Ingrese el nombre de la nueva proteína a agregar o modificar")
    new_key = input("Nombre: ")
    new_key = new_key.upper()
    print("Ingrese la descripción de la proteína")
    new_value = input("Descripción: ")

    if new_key not in prot_base:
        prot_base[new_key] = new_value
    else:
        print("Este elemento ya existe en la base de datos")
        print("Si desea editar el elemento elija la opción (2)")
    print('\n')

    return prot_base


#   Editar un elemento ya existente en la base de datos
def edit_elements(prot_base):

    edit_element = input("Ingrese la proteína que desea editar: ")
    edit_element = edit_element.upper()

    if edit_element in prot_base:
        print("Ingrese la nueva descrición para la proteína a modificar")
        edit_value = input("Descripción: ")
        prot_base[edit_element] = edit_value

    else:
        print("El elemento ingresado no existe, intente nuevamente")

    print('\n')
    return prot_base


#   Eliminar el valor correspondiente a una proteína ya existente
def dell_values(prot_base):
    print("Ingrese el nombre de la proteína para eliminar su", end=" ")
    print("valor en la base de datos")
    print('\n')
    remove_value = input("Protenina: ")
    remove_value = remove_value.upper()

    if remove_value in prot_base:
        prot_base[remove_value] = ''

    else:
        print("La proteína ingresada no existe (su valor tampoco ¬_¬)")
    print('\n')
    return prot_base


#   Eliminar un elemento por completo de la base de datos
def dell_elements(prot_base):

    dell_key = input("Nombre: ")
    dell_key = dell_key.upper()

    if dell_key in prot_base:
        del prot_base[dell_key]
        print("Se ha eliminado exitosamente la proteína: ", dell_key)
    else:
        print("No es posible eliminar un elemento que no existe")
    print('\n')

    return prot_base


#   Impresión de la base de datos con sus keys y values
def data(prot_base):
    for key, value in prot_base.items():
        print(key, value)

    print('\n')


#   Menú de acciones
def menu():

    print("Menú de acciones para base experimental de datos proteínas")
    print('\n')
    print("(1) Agregar elementos al diccionario")
    print("(2) Editar elementos en el diccionario")
    print("(3) Obtenga el valor para la proteína (key) que desee buscar")
    print("(4) Elimine algun valor de una proteína", end=" ")
    print("perteneciente al diccionario")
    print("(5) Imprimir todos los valores almacenados en el diccionario")
    print("(6) Elimine un elemento del diccionario")
    print("(7) Salir de la base de datos de proteínas")

if __name__ == '__main__':

    prot_base = {
        '6S1A': 'Ligand binding domain of the P. putida- receptor PcaY_PP',
        '6S96': 'Crystal structure of the catalytic domain of UBE2S C118A.',
        '6S98': 'Crystal structure of the catalytic domain of UBE2S WT.',
        '6LPG': 'human VASH1-SVBP complex',
        '4GWM': 'Crystal structure of human promeprin beta',
        '4A5G': 'Raphanus sativus anionic peroxidase.',
        '5GW4': 'Structure of Yeast NPP-TRiC',
        '3HN3': 'Human beta-glucuronidase at 1.7 A resolution',
    }
    system('clear')
    while True:

        menu()
        print('\n')
        choice = input("Ingrese la acción que desee realizar: ")

        if choice == "1":
            system('clear')
            print("Agregue elementos a la base de datos (uno a uno)")
            prot_base = add_elements(prot_base)

        elif choice == "2":
            system('clear')
            print("Edite algún elemento de la base de datos")
            print('\n')
            prot_base = edit_elements(prot_base)

        elif choice == "3":
            system('clear')
            print("Imprimir información de proteína a elección")
            print('\n')
            print_description(prot_base)

        elif choice == "4":
            system('clear')
            prot_base = dell_values(prot_base)

        elif choice == "5":
            system('clear')
            print("Datos contenidos en el diccionario: \n")
            print('\n')
            data(prot_base)

        elif choice == "6":
            system('clear')
            print("Elija el elemento que quiera eliminar del diccionario")
            print('\n')
            prot_base = dell_elements(prot_base)

        elif choice == "7":
            system('clear')
            print("Ha deseado salir del programa, adios ")
            break
        else:
            system('clear')
            print("El valor ingresado no entrega ninguna respuesta", end=" ")
            print(" ingrese una opción valida")
            print('\n')
